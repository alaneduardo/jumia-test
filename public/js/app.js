// Define the `jumiaTestApp` module
var jumiaTestApp = angular.module('jumiaTestApp', []);

jumiaTestApp.filter('split', function() {
    return function(input, splitChar, splitIndex) {
        // do some bounds checking here to ensure it has that index
        return input.split(splitChar)[splitIndex];
    }
});

// Define the `CustomerController` controller on the `jumiaTestApp` module
jumiaTestApp.controller('CustomerController', function CustomerController($scope, $http) {
  // Obtaining the customer list
  $http.get('/api/customer').then(function(response) {
    $scope.customers = response.data.data;
  });

  // Obtaining the country list
  $http.get('/api/country').then(function(response) {
    $scope.countries = response.data.data;
  });

  // Method to check if it is empty to reset the country filter
  $scope.checkEmptyCountry = function (combo) {
    if(combo.value === "")
      delete $scope.search.country.id;
  }

  // Method to check if it is empty to reset the valid phone number filter
  $scope.checkEmptyFilter = function (combo) {
    if(combo.value === "")
      delete $scope.search.isValidPhone;
  }
});
