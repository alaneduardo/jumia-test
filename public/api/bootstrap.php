<?php
declare(strict_types=1);
/**
 * Project backend bootstrap
 *
 * This file is the bootstrap of backend project.
 *
 * @author Alan Eduardo <alan_eduardo@outlook.com.br>
 * @version 1.0
 * @package JumiaTest
 */

/** All backend responses must be a JSON file */
header('Content-Type: application/json');

/** The errors must be disabled because they are handled by shutdown function in src/autoload.php */
ini_set('display_errors', 'on');

/** Requires the autoload.php file to register automatic class calls and error handling */
require_once("../../src/autoload.php");

/** Requires the error_handler.php file to register the error handling */
require_once("../../src/error_handler.php");

/** Requires the routes.php file to register the Routes */
require_once("../../src/Http/routes.php");
