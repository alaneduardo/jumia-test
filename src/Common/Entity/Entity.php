<?php
/**
 * Abstract class Entity definition
 *
 * @author Alan Eduardo <alan_eduardo@outlook.com.br>
 * @version 1.0
 * @package JumiaTest\Common\Entity
 */

namespace JumiaTest\Common\Entity;

abstract class Entity implements \JsonSerializable {
  /**
   * Magic method to getters and setters Entity class
   * @param  string $method method called
   * @param  array $params parameters sended to the method called
   * @return mixed
   */
   function __call($method, $params) {
     if((strncasecmp($method, "get", 3) === 0) || (strncasecmp($method, "set", 3) === 0)) {
       $var = lcfirst(substr($method, 3));

       if (strncasecmp($method, "get", 3) === 0) {
         return $this->$var;
       }
       if (strncasecmp($method, "set", 3) === 0) {
         $this->$var = $params[0];
       }
     }
   }

  /**
   * Method to serialize the object to a valid JSON including the protected attributes
   * @return array Object converted into a serialized array
   */
  public function jsonSerialize() {
    $json = array();
    foreach($this as $key => $value) {
        $json[$key] = $value;
    }
    return $json;
  }
}
