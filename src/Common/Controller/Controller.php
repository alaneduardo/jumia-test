<?php
/**
 * Class Controller definition
 *
 * @author Alan Eduardo <alan_eduardo@outlook.com.br>
 * @version 1.0
 * @package JumiaTest\Libs
 */

namespace JumiaTest\Common\Controller;

/**
 * Definition for the abstract controller class
 *
 * This class contains common methods to be used in all controllers in the project
 */
abstract class Controller {

  /**
  * Outputs a JSON on the HTTP response
  *
  * Receives an array or object and converts it to a JSON and responses to HTTP.
  *
  * @param array $data Structured data that will be converted to a JSON
  * @return string JSON string
  */
  protected function exportJSON(array $data): string {
    return json_encode($data);
  }
}
