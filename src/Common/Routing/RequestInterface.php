<?php
/**
 * Interface Request definition
 *
 * @author Alan Eduardo <alan_eduardo@outlook.com.br>
 * @version 1.0
 * @package JumiaTest\Common\Routing
 */

namespace JumiaTest\Common\Routing;

interface RequestInterface {
    public function getBody();
}
