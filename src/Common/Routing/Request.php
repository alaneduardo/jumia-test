<?php
/**
 * Class Request definition
 *
 * @author Alan Eduardo <alan_eduardo@outlook.com.br>
 * @version 1.0
 * @package JumiaTest\Common\Routing
 */

namespace JumiaTest\Common\Routing;

class Request implements RequestInterface {
  function __construct() {
    $this->bootstrapSelf();
  }

  /**
   * The class bootstraping method
   */
  private function bootstrapSelf():void {
    foreach($_SERVER as $key => $value) {
      $this->{$this->toCamelCase($key)} = $value;
    }
  }

  /**
   * Method to parse the string sended to camel case
   * @param  string $string String that must be parsed
   * @return string         Parsed string
   */
  private function toCamelCase(string $string):string {
    $result = strtolower($string);

    preg_match_all('/_[a-z]/', $result, $matches);
    foreach($matches[0] as $match) {
        $c = str_replace('_', '', strtoupper($match));
        $result = str_replace($match, $c, $result);
    }
    return $result;
  }

  /**
   * Gets the HTTP Body based on the verb
   * @return array HTTP body parsed
   */
  public function getBody() {
    if($this->requestMethod === "GET") {
      return;
    }
    if ($this->requestMethod == "POST") {
      $result = array();
      foreach($_POST as $key => $value) {
        $result[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
      }
      return $result;
    }
  }
}
