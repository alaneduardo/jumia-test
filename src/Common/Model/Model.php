<?php
/**
 * Class Model definition
 *
 * @author Alan Eduardo <alan_eduardo@outlook.com.br>
 * @version 1.0
 * @package JumiaTest\Libs
 */

namespace JumiaTest\Common\Model;

/**
 * Definition for the abstract model class
 *
 * This class contains common methods to be used in all models in the project
 */
abstract class Model {

  /**
   * The database connection. Protected
   * @var $db PDO
   */
  protected $db;

  /**
  * Class constructor
  *
  * On the class construction, creates a database conection.
  *
  * @throws Exception
  * @return void
  */
  function __construct() {
    $this->bootstrapSelf();
  }

  /**
   * Boostraps a connection
   */
  function bootstrapSelf():void {
    global $MAIN_CONFIG;

    $this->db = new \PDO($MAIN_CONFIG['db']);
    $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    $this->db->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
  }

  /**
   * get the current DB connection
   * @return \PDO Current Connection
   */
  function getDB():\PDO {
    return $this->db;
  }
}

 ?>
