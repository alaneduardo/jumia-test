<?php
/**
 * Class entity Country definition
 *
 * @author Alan Eduardo <alan_eduardo@outlook.com.br>
 * @version 1.0
 * @package JumiaTest\PhoneValidating\Country
 */

namespace JumiaTest\PhoneValidating\Country;

use JumiaTest\Common\Entity\Entity;

/**
 * Class entity Country definition
 */
class CountryEntity extends Entity {
  /**
   * Country id in the database
   * @var int
   */
  protected $id;
  /**
   * Country name
   * @var string
   */
  protected $name;
  /**
   * Country phone rule to match with a phone number
   * @var string
   */
  protected $phoneRule;
  /**
   * Country code number
   * @var int
   */
  protected $code;

  /**
   * getter for property Id
   * @return int Id
   */
  public function getId():int {
    return $this->id;
  }
  /**
   * Setter for property Id
   * @param int $id Id
   */
  public function setId(int $id) {
    $this->id = $id;
  }

  /**
   * getter for property Name
   * @return string Name
   */
  public function getName():string {
    return $this->name;
  }
  /**
   * Setter for property Name
   * @param string $name Name
   */
  public function setName(string $name) {
    $this->name = $name;
  }

  /**
   * getter for property Phone Rule
   * @return string Phone Rule
   */
  public function getPhoneRule():string {
    return $this->phoneRule;
  }
  /**
   * Setter for property Phone Rule
   * @param string $phoneRule Phone Rule
   */
  public function setPhoneRule(string $phoneRule) {
    if(@preg_match("/".$phoneRule."/", null) === false) {
      throw new \InvalidArgumentException("Error Processing Request. Bad regular expression");
    }

    $this->phoneRule = $phoneRule;
  }

  /**
   * getter for property Code
   * @return int Code
   */
  public function getCode():int {
    return $this->code;
  }
  /**
   * Setter for property Code
   * @param int $code Code
   */
  public function setCode(int $code) {
    $this->code = $code;
  }

  /**
   * Validates a phone using the country phone rule
   * @param  string $phone Testing phone number
   * @return bool          If the phone number is valid will return true. Case the number phone is invalid will return false
   */
  public function validatePhone(string $phone): bool {
    return (bool) preg_match("/".$this->getPhoneRule()."/", $phone);
  }
}
