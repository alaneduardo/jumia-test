<?php
/**
 * Class Country Controller definition
 *
 * @author Alan Eduardo <alan_eduardo@outlook.com.br>
 * @version 1.0
 * @package JumiaTest\PhoneValidating\Country
 */
namespace JumiaTest\PhoneValidating\Country;

use JumiaTest\Common\Controller\Controller;

/**
 * Definition for the country controller class
 *
 * This class contains the controller methods for country.
 */
class CountryController extends Controller {
  /**
   * Obtains a full list of countries.
   * @return string JSON string
   */
  public function index():string {
    $country = new CountryModel();

    return self::exportJSON([
      'status' => 'ok',
      'data' => $country->findAll()
    ]);
  }
}
