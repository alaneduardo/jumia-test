<?php
/**
 * Class Country Model definition
 *
 * @author Alan Eduardo <alan_eduardo@outlook.com.br>
 * @version 1.0
 * @package JumiaTest\PhoneValidating\Country
 */

namespace JumiaTest\PhoneValidating\Country;

use JumiaTest\Common\Model\Model;

/**
 * Definition for the Country Model class
 *
 * This class contains all business rules for the Country Model
 */
class CountryModel extends Model {
  /**
   * Method to findAll the registers in the country table.
   * @return array list of all countries
   */
  public function findAll():array {
    $result = $this->db->query(" SELECT id, name, regex as `phoneRule`, code FROM country ");

    $ret = $result ? $result->fetchAll(\PDO::FETCH_CLASS,"JumiaTest\\PhoneValidating\\Country\\CountryEntity") : [];

    return $ret;
  }
}
