<?php
/**
 * Class Customer Model definition
 *
 * @author Alan Eduardo <alan_eduardo@outlook.com.br>
 * @version 1.0
 * @package JumiaTest\PhoneValidating\Customer
 */

namespace JumiaTest\PhoneValidating\Customer;

use JumiaTest\Common\Model\Model;

/**
 * Definition for the Customer Model class
 *
 * This class contains all business rules for the Customer Model
 */
class CustomerModel extends Model {
  /**
   * Method to findAll the registers in the customer table.
   * @return array list of all customers
   */
  public function findAll():array {
    $result = $this->db->query("
    SELECT c1.id, c1.name, c1.phone,
           c2.id as `countryId`, c2.name as `countryName`, c2.regex as `countryPhoneRule`, c2.code as `countryCode`
      FROM customer c1
           LEFT JOIN country c2 ON (c1.phone like '('||c2.code||')%');
    ");

    $ret = $result ? $result->fetchAll(\PDO::FETCH_CLASS, 'JumiaTest\\PhoneValidating\\Customer\\CustomerEntity') : [];

    return $ret;
  }
}
