<?php
/**
 * Class entity Customer definition
 *
 * @author Alan Eduardo <alan_eduardo@outlook.com.br>
 * @version 1.0
 * @package JumiaTest\PhoneValidating\Customer
 */

namespace JumiaTest\PhoneValidating\Customer;

use JumiaTest\Common\Entity\Entity;
use JumiaTest\PhoneValidating\Country\CountryEntity;

/**
 * Class entity Customer definition
 */
class CustomerEntity extends Entity {
  /**
   * Customer id in the database
   * @var integer
   */
  protected $id;
  /**
   * Customer name
   * @var string
   */
  protected $name;
  /**
   * Customer country object
   * @var Country
   */
  protected $country;
  /**
   * Customer code number
   * @var string
   */
  protected $phone;
  /**
   * Flag if the customer phone is valid
   * @var bool
   */
  protected $isValidPhone;

  /**
   * Class constructor
   * @param integer $id        Customer id
   * @param string  $name      Customer name
   * @param string  $phoneRule A valid regexp to validate phone numbers
   * @param integer $code      Customer phone code
   */
  public function __construct() {
    if(!empty($this->phone)) {
      $this->bootstrapAuto();
    }
  }

  /**
   * Auto bootstraping class via PDO fetchObject constructor
   */
  private function bootstrapAuto(): void {
    $this->country = new CountryEntity();

    $this->country->setId((int) $this->countryId);
    $this->country->setName($this->countryName);
    $this->country->setPhoneRule($this->countryPhoneRule);
    $this->country->setCode((int) $this->countryCode);

    $this->setIsValidPhone($this->country->validatePhone($this->phone));

    unset($this->countryCode, $this->countryPhoneRule, $this->countryName, $this->countryId);
  }



  /**
   * getter for property Id
   * @return int Id
   */
  public function getId():int {
    return $this->id;
  }
  /**
   * Setter for property Id
   * @param int $id Id
   */
  public function setId(int $id) {
    $this->id = $id;
  }

  /**
   * getter for property Name
   * @return string Name
   */
  public function getName():string {
    return $this->name;
  }
  /**
   * Setter for property Name
   * @param string $name Name
   */
  public function setName(string $name) {
    $this->name = $name;
  }

  /**
   * getter for property country entity
   * @return string Country Entity
   */
  public function getCountry():CountryEntity {
    return $this->country;
  }
  /**
   * Setter for property Country Entity
   * @param string $phoneRule Country Entity
   */
  public function setCountry(CountryEntity $country) {
    $this->country = $country;
  }

  /**
   * getter for property isValidPhone
   * @return bool
   */
  public function getIsValidPhone():bool {
    return $this->isValidPhone;
  }
  /**
   * Setter for property isValidPhone
   * @param bool $isValidPhone isValidPhone
   */
  public function setIsValidPhone(bool $isValidPhone) {
    $this->isValidPhone = $isValidPhone;
  }
}
