<?php
/**
 * Class Customer Controller definition
 *
 * @author Alan Eduardo <alan_eduardo@outlook.com.br>
 * @version 1.0
 * @package JumiaTest\PhoneValidating\Customer
 */

namespace JumiaTest\PhoneValidating\Customer;

use JumiaTest\Common\Controller\Controller;

/**
 * Definition for the customer controller class
 *
 * This class contains the controller methods for customers.
 */
class CustomerController extends Controller {
  /**
   * Obtains a full list of customers.
   * @return string JSON string
   */
  public function index():string {
    $customer = new CustomerModel();
    
    return self::exportJSON([
      'status' => 'ok',
      'data' => $customer->findAll()
    ]);
  }
}
