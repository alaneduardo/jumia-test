<?php

use JumiaTest\Common\Routing\Router;

$router = new Router();

$router->get('/api/country', 'JumiaTest\\PhoneValidating\\Country\\CountryController::index');
$router->get('/api/customer', 'JumiaTest\\PhoneValidating\\Customer\\CustomerController::index');
