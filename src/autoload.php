<?php
/**
 * Project autoload classes
 *
 * This file has the functions to register the called classes
 *
 * @author Alan Eduardo <alan_eduardo@outlook.com.br>
 * @version 1.0
 * @package JumiaTest
 */

$env = strtolower(getenv("APP_ENV"));

/** Retrieves the app config based the e */
$MAIN_CONFIG = include_once(dirname(dirname(__FILE__))."/config/app.$env.php");

/**
* Autoload function to load the system classes
*
* This function tries load the class by name. It tries to load in the src folder matching by namespace.
*
* @param string $className The class name that be loaded by the system.
* @throws Exception
* @return void
*/
function autoloadFunction(string $className):void {
  if(strpos($className, 'JumiaTest') !== false) {
    $folder = str_replace('JumiaTest', 'src', $className);

    if(file_exists(str_replace('\\', '/', dirname(dirname(__FILE__)).'/' . $folder . '.php')))
      require_once(str_replace('\\', '/', dirname(dirname(__FILE__)).'/' . $folder . '.php'));
    else
      throw new Exception("Unable to load $className class on the folder $folder");
  }
}
spl_autoload_register('autoloadFunction');
