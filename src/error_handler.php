<?php
/**
 * Project error handling
 *
 * This file has the functions to register the errors handling
 *
 * @author Alan Eduardo <alan_eduardo@outlook.com.br>
 * @version 1.0
 * @package JumiaTest
 */

/**
* Catches an error and outputs as JSON
*
* This function catches the PHP error and send as JSON.
*
* @return void
*/
function shutdownFunction() {
  $error = error_get_last();

  if(!empty($error))
    echo json_encode([ 'status' => 'error', 'data' => $error ]);
}
register_shutdown_function('shutdownFunction');
