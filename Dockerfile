FROM php:7.3.1-apache

RUN a2enmod rewrite

RUN mkdir -p /var/www && \
cd /var/www && apt-get update && \
apt-get install wget -y && \
wget -O phpunit https://phar.phpunit.de/phpunit-8.phar && \
chmod +x phpunit

CMD ["apache2-foreground"]
