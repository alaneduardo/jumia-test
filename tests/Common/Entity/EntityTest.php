<?php
declare(strict_types=1);

use JumiaTest\Common\Tests\TestCase;
use JumiaTest\Common\Entity\Entity;

class EntityTest extends TestCase {

    public function testSetsAndGetsAAttributeValue(): void {
      $stub = $this->getMockForAbstractClass(Entity::class);

      $stub->setName('Test Name');
      $this->assertEquals('Test Name', $stub->getName());

      $stub->setIsChecked(true);
      $this->assertEquals(true, $stub->getIsChecked());

      $stub->setAge(32);
      $this->assertEquals(32, $stub->getAge());
    }


    public function testSerializingObject(): void {
      $stub = $this->getMockForAbstractClass(Entity::class);

      $stub->setProductName('Test Product');
      $stub->setPrice(50);
      $stub->setCurrency('€');

      $this->assertEquals(
        '{"productName":"Test Product","price":50,"currency":"\u20ac"}',
        json_encode($stub)
      );
    }
}
