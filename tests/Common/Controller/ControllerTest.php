<?php
declare(strict_types=1);

use JumiaTest\Common\Tests\TestCase;
use JumiaTest\Common\Controller\Controller;

class ControllerTest extends TestCase {

    public function testExportsAValidString(): void {
      $stub = $this->getMockForAbstractClass(Controller::class);

      $this->assertIsString($this->invokeMethod($stub, 'exportJSON', [['test' => 'ok']]));
    }

    public function testExportsAValidJson(): void {
      $stub = $this->getMockForAbstractClass(Controller::class);

      $this->assertEquals('{"test":"ok"}', $this->invokeMethod($stub, 'exportJSON', [['test' => 'ok']]));
    }

    public function testHandlesAInvalidData(): void {
      $this->expectException(\TypeError::class);

      $stub = $this->getMockForAbstractClass(Controller::class);
      $this->invokeMethod($stub, 'exportJSON', [1]);
    }
}
