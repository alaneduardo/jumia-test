<?php
declare(strict_types=1);

use JumiaTest\Common\Tests\TestCase;
use JumiaTest\Common\Model\Model;

class ModelTest extends TestCase {

    public function testCreatesAValidConnect(): void {
      $stub = $this->getMockForAbstractClass(Model::class);

      $this->assertInstanceOf(\PDO::class, $stub->getDB());
    }
}
