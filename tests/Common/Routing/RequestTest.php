<?php
declare(strict_types=1);

use JumiaTest\Common\Tests\TestCase;
use JumiaTest\Common\Routing\Request;

class RequestTest extends TestCase {

    public function testParsingToCamelCase(): void {
      $request = new Request();

      $this->assertEquals(
        'testRequestCamelCase',
        $this->invokeMethod($request, 'toCamelCase', ['test_request_camel_case'])
      );

      $this->assertEquals(
        'test',
        $this->invokeMethod($request, 'toCamelCase', ['test'])
      );

      $this->assertEquals(
        'testRequest',
        $this->invokeMethod($request, 'toCamelCase', ['test_request'])
      );
    }

    public function testInvalidParameterHandling():void {
      $this->expectException(\TypeError::class);

      $request = new Request();
      $this->invokeMethod($request, 'toCamelCase', [new stdClass]);
    }
}
