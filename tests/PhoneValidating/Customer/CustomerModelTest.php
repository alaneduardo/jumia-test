<?php
declare(strict_types=1);

use JumiaTest\Common\Tests\TestCase;
use JumiaTest\PhoneValidating\Customer\CustomerModel;

class CustomerModelTest extends TestCase {

    public function testReturningAnArray(): void {
      $model = new CustomerModel();
      $this->assertIsArray($model->findAll());
    }

    public function testReturningAValidData(): void {
      $model = new CustomerModel();
      $this->assertNotNull($model->findAll());
    }
}
