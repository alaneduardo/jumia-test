<?php
declare(strict_types=1);

use JumiaTest\Common\Tests\TestCase;
use JumiaTest\PhoneValidating\Customer\CustomerEntity;
use JumiaTest\PhoneValidating\Country\CountryEntity;

class CustomerEntityTest extends TestCase {

    public function testSetCustomerId(): void {
      $entity = new CustomerEntity();
      $entity->setId(100);

      $this->assertEquals(100, $entity->getId());
    }

    public function testHandleInvalidCustomerId(): void {
      $this->expectException(\TypeError::class);

      $entity = new CustomerEntity();
      $entity->setId('test');
    }

    public function testSetCustomerName(): void {
      $entity = new CustomerEntity();
      $entity->setName('Mandi Khabala');

      $this->assertEquals('Mandi Khabala', $entity->getName());
    }

    public function testHandleInvalidCustomerName(): void {
      $this->expectException(\TypeError::class);

      $entity = new CustomerEntity();
      $entity->setName(false);
    }

    public function testSetCustomerCountry(): void {
      $entity = new CustomerEntity();

      $entity->setCountry(new CountryEntity());

      $this->assertInstanceOf(CountryEntity::class, $entity->getCountry());
    }

    public function testHandleInvalidCountry(): void {
      $this->expectException(\TypeError::class);

      $entity = new CustomerEntity();
      $entity->setCountry('Portugal');
    }

    public function testSetCustomerIsValidPhone(): void {
      $entity = new CustomerEntity();
      $entity->setIsValidPhone(true);

      $this->assertEquals(true, $entity->getIsValidPhone());
    }

    public function testHandleInvalidCustomerCode(): void {
      $this->expectException(\TypeError::class);

      $entity = new CustomerEntity();
      $entity->setIsValidPhone('false');
    }

    public function testVerifyMethodValidatePhone(): void {
      $country = new CountryEntity();

      $country->setName('Cameroon');
      $country->setPhoneRule('\(237\)\ ?[2368]\d{7,8}$');

      $entity = new CustomerEntity();
      $entity->setCountry($country);

      $tests = [
        'test' => false,
        '(237) 697151594' => true,
        '(237) 9969715159' => false
      ];

      foreach($tests as $phone => $expects) {
        $this->assertEquals($expects, $entity->getCountry()->validatePhone($phone));
      }//
    }
}
