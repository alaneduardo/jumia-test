<?php
declare(strict_types=1);

use JumiaTest\Common\Tests\TestCase;
use JumiaTest\PhoneValidating\Customer\CustomerController;

final class CustomerControllerTest extends TestCase
{
    protected $controller;

    protected function setUp(): void {
        $this->controller = new CustomerController();
    }

    public function testIndexGeneratesAValidJson(): void {
      $response = $this->controller->index();
      $this->assertNotNull($response);
    }

    public function testIndexGeneratesAValidString(): void {
      $response = $this->controller->index();
      $this->assertIsString($response);
    }
}
