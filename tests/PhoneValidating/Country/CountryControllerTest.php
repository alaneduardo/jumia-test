<?php
declare(strict_types=1);

use JumiaTest\Common\Tests\TestCase;
use JumiaTest\PhoneValidating\Country\CountryController;

class CountryControllerTest extends TestCase
{
    protected $controller;

    protected function setUp(): void {
      $this->controller = new CountryController();
    }

    public function testIndexGeneratesAValidJson(): void {
      $response = $this->controller->index();
      $this->assertNotNull($response);
    }

    public function testIndexGeneratesAValidString(): void{
      $response = $this->controller->index();
      $this->assertIsString($response);
    }
}
