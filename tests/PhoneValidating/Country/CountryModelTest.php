<?php
declare(strict_types=1);

use JumiaTest\Common\Tests\TestCase;
use JumiaTest\PhoneValidating\Country\CountryModel;

class CountryModelTest extends TestCase {

    public function testReturningAnArray(): void {
      $model = new CountryModel();
      $this->assertIsArray($model->findAll());
    }

    public function testReturningAValidData(): void {
      $model = new CountryModel();
      $this->assertNotNull($model->findAll());
    }
}
