<?php
declare(strict_types=1);

use JumiaTest\Common\Tests\TestCase;
use JumiaTest\PhoneValidating\Country\CountryEntity;

class CountryEntityTest extends TestCase {

    public function testSetCountryId(): void {
      $entity = new CountryEntity();
      $entity->setId(100);

      $this->assertEquals(100, $entity->getId());
    }

    public function testHandleInvalidCountryId(): void {
      $this->expectException(\TypeError::class);

      $entity = new CountryEntity();
      $entity->setId('test');
    }

    public function testSetCountryName(): void {
      $entity = new CountryEntity();
      $entity->setName('Portugal');

      $this->assertEquals('Portugal', $entity->getName());
    }

    public function testHandleInvalidCountryName(): void {
      $this->expectException(\TypeError::class);

      $entity = new CountryEntity();
      $entity->setName(false);
    }

    public function testSetCountryPhoneRule(): void {
      $entity = new CountryEntity();
      $entity->setPhoneRule('\(237\)\ ?[2368]\d{7,8}$');

      $this->assertEquals('\(237\)\ ?[2368]\d{7,8}$', $entity->getPhoneRule());
    }

    public function testHandleInvalidPhoneRule(): void {
      $this->expectException(\InvalidArgumentException::class);

      $entity = new CountryEntity();
      $entity->setPhoneRule('\(237\)\ ?[2368\d{7,8}$');
    }

    public function testSetCountryCode(): void {
      $entity = new CountryEntity();
      $entity->setCode(212);

      $this->assertEquals(212, $entity->getCode());
    }

    public function testHandleInvalidCountryCode(): void {
      $this->expectException(\TypeError::class);

      $entity = new CountryEntity();
      $entity->setCode('test');
    }

    public function testVerifyMethodValidatePhone(): void {
      $entity = new CountryEntity();
      $entity->setPhoneRule('\(237\)\ ?[2368]\d{7,8}$');

      $tests = [
        'test' => false,
        '(237) 697151594' => true,
        '(237) 9969715159' => false
      ];

      foreach($tests as $phone => $expects) {
        $this->assertEquals($expects, $entity->validatePhone($phone));
      }
    }
}
