# Welcome to the Jumia Services Test

Below you will find all information about the project, your structure and suit test.

## Table of Contents

* [Requirements](#markdown-header-requirements)
* [Getting Started](#markdown-header-getting-started)
* [Folder Structure](#markdown-header-folder-structure)
    * [/bin](#markdown-header-bin-folder)
    * [/config](#markdown-header-config-folder)
    * [/data](#markdown-header-data-folder)
    * [/docs](#markdown-header-docs-folder)
    * [/public](#markdown-header-public-folder)
        * [/api](#markdown-header-api-folder)
    * [/src](#markdown-header-src-folder)
    * [/tests](#markdown-header-tests-folder)
* [Run Tests](#markdown-header-run-tests)

## Requirements

You must be installed Docker and Docker Compose on your machine. Read the [Docker installation guide](https://docs.docker.com/install/) and [Docker Composer installation guide](https://docs.docker.com/compose/install/)


## Getting Started

To run this project you must run the commands below on the project root folder:

To run with the development environment
```
$ ./bin/run DEV
```

To run with the development environment
```
$ ./bin/run PRD
```

The project will be accessible in [http://localhost:3000](http://localhost:3000)

## Folder Structure

This project uses a construction pattern MVC and it is divided with these folders: data, docs, public, src and tests.

### Bin Folder

The bin folder has all bash scripts to run automated tasks

### Config Folder

The bin config has all config files by environment type

### Data Folder

The data folder has all scripts and database files and migrations used by the project.

### Docs Folder

The docs folder has the project documentation and specs.

### Public folder

The public folder is where are all frontend and the REST API bootstrap.

#### Api Folder

The api folder has the bootstrap backend REST API call.

### Src folder

The src folder has all the backend classes: Libs, Models (Entities) and Controllers.

### Tests folder

The tests folder has all phpUnit test files.

## Run Tests

To run the test is used the PHPUnit test suit.

Command to run the tests
```
$ ./bin/test
```
